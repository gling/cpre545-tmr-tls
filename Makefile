all: build/cert.key build/echo_server build/bad_echo_server build/forward_server build/voter_server

build/cert.key build/cert.crt:
	mkdir -p build
	openssl req -x509 -newkey rsa:4096 -keyout build/cert.key -out build/cert.crt -days 365 -nodes -subj '/'

# cert.key.o: cert.key
# 	ld -r -b binary -o cert.key.o cert.key

# cert.crt.o: cert.crt
# 	ld -r -b binary -o cert.key.o cert.key

build/echo_server: src/echo_server.c src/v_sock.c Makefile
	mkdir -p build
	gcc src/echo_server.c src/v_sock.c -O3 -flto -lssl -lcrypto --static -o build/echo_server

build/bad_echo_server: src/bad_echo_server.c src/v_sock.c Makefile
	mkdir -p build
	gcc src/bad_echo_server.c src/v_sock.c -O3 -flto -lssl -lcrypto --static -o build/bad_echo_server

build/forward_server: src/forward_server.c src/v_sock.c Makefile
	mkdir -p build
	gcc src/forward_server.c src/v_sock.c -O3 -flto -lssl -lcrypto --static -o build/forward_server

build/voter_server: src/voter_server.c src/v_sock.c Makefile
	mkdir -p build
	gcc src/voter_server.c src/v_sock.c -O3 -flto -lssl -lcrypto --static -o build/voter_server