#ifndef V_SOCK_H_
#define V_SOCK_H_

#include <openssl/ssl.h>
#include <sys/epoll.h>
#include <stdbool.h>

#define MAX_SSL_RECORD 16384
#define EPOLL_LOOP_BATCH_SIZE 1

struct epoll_entry;

typedef void (*epoll_callback)(int epoll, struct epoll_entry *);

struct epoll_entry {
  epoll_callback in;
  epoll_callback out;
  epoll_callback hup;
  int events;
  int socket;
};

struct epoll_server {
  struct epoll_entry epoll;
  SSL_CTX * ssl;
};

struct epoll_conn {
  struct epoll_entry epoll;
  SSL * ssl;
};

SSL_CTX * client_ssl_ctx_create();

int v_listen(struct epoll_server * server, const char * crt, const char * key, int port);
int v_listen_close(struct epoll_server * server);
int v_accept(const struct epoll_server * server, struct epoll_conn * sock);

int v_connect(struct epoll_conn * conn, SSL_CTX * ssl, const char * host, int port);
int v_read(struct epoll_conn * sock, void * data, size_t len);
int v_write(struct epoll_conn * sock, const void * data, size_t len);
int v_close(struct epoll_conn * conn);

void v_epoll_mod_enable(int epoll, struct epoll_entry * entry, int epoll_flags);
void v_epoll_mod_disable(int epoll, struct epoll_entry * entry, int epoll_flags);
void v_epoll_add(int epoll, struct epoll_entry * entry, int epoll_flags);
void v_epoll_del(int epoll, struct epoll_entry * entry);

int v_epoll_run_loop(int epoll);

#endif