// Reference https://github.com/openssl/openssl/blob/master/demos/sslecho/main.c

#include "v_sock.h"

#include <arpa/inet.h>
#include <openssl/err.h>
#include <signal.h>
#include <stdbool.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <unistd.h>

static SSL_CTX * server_ssl_ctx_create(const char * crt, const char * key) {
  SSL_CTX * ctx = SSL_CTX_new(TLS_server_method());
  if (ctx == NULL) {
    ERR_print_errors_fp(stderr);
    return NULL;
  }

  if (SSL_CTX_use_certificate_chain_file(ctx, crt) <= 0) {
    ERR_print_errors_fp(stderr);
    return NULL;
  }

  if (SSL_CTX_use_PrivateKey_file(ctx, key, SSL_FILETYPE_PEM) <= 0) {
    ERR_print_errors_fp(stderr);
    return NULL;
  }

  return ctx;
}

SSL_CTX * client_ssl_ctx_create() {
  SSL_CTX * ctx = SSL_CTX_new(TLS_client_method());
  if (ctx == NULL) {
    ERR_print_errors_fp(stderr);
    return NULL;
  }

  // Trust all certificates (for testing)
  // Change to SSL_VERIFY_PEER and add trusted SSL certificates in production.
  SSL_CTX_set_verify(ctx, SSL_VERIFY_NONE, NULL); // SSL_VERIFY_PEER

  return ctx;
}

static int server_listen(int port) {
  signal(SIGPIPE, SIG_IGN); // Ignore SIGPIPE

  int sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0) {
    perror("Socket open failed");
    return -1;
  }

  struct sockaddr_in addr = {
    .sin_family = AF_INET,
    .sin_port   = htons(port),
    .sin_addr   = { .s_addr = INADDR_ANY },
  };

  /* Reuse the address; good for quick restarts */
  int yes = 1;
  if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(yes)) < 0) {
    perror("setsockopt(SO_REUSEADDR) failed");
    return -1;
  }

  if (bind(sock, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
    perror("Unable to bind");
    return -1;
  }

  if (listen(sock, 1) < 0) {
    perror("Unable to listen");
    return -1;
  }

  return sock;
}

static int client_connect(const char * host, int port) {
  int sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0) {
    perror("Socket open failed");
    return -1;
  }


  struct sockaddr_in addr = {
    .sin_family = AF_INET,
    .sin_port   = htons(port),
    .sin_addr   = { .s_addr = inet_addr(host) },
  };

  if (connect(sock, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
    perror("connect() failed");
    return -1;
  }

  return sock;
}


int v_listen(struct epoll_server * server, const char * crt, const char * key, int port) {
  if (crt) {
    server->ssl = server_ssl_ctx_create(crt, key);
    if (server->ssl == NULL) {
      perror("SSL CTX is NULL");
      return -1;
    }
  } else {
    server->ssl = NULL;
  }

  server->epoll.socket = server_listen(port);
  if (server->epoll.socket < 0) {
    perror("Listen error");
    return -1;
  }

  return 0;
}

int v_listen_close(struct epoll_server * server) {
  if (close(server->epoll.socket) < 0) {
    perror("Closing socket");
    return -1;
  }
  server->epoll.socket = 0;

  if (server->ssl) {
    SSL_CTX_free(server->ssl);
    server->ssl = NULL;
  }

  return 0;
}

int v_accept(const struct epoll_server * server, struct epoll_conn * conn) {
  if ((conn->epoll.socket = accept(server->epoll.socket, NULL, NULL)) < 0) {
    perror("Server accept failed");
    return -1;
  }

  if (server->ssl) {
    conn->ssl = SSL_new(server->ssl); // OOM?
    SSL_set_fd(conn->ssl, conn->epoll.socket);

    if (SSL_accept(conn->ssl) < 0) {
      perror("SSL accept failed");
      return -1;
    }
  } else {
    conn->ssl = NULL;
  }

  return 0;
}

int v_connect(struct epoll_conn * conn, SSL_CTX * ssl, const char * host, int port) {
  conn->epoll.socket = client_connect(host, port);
  if (conn->epoll.socket < 0) return -1;

  if (ssl) {
    conn->ssl = SSL_new(ssl);
    if (SSL_set_fd(conn->ssl, conn->epoll.socket) <= 0) {
      ERR_print_errors_fp(stderr);
      return -1;
    }

    if (SSL_connect(conn->ssl) <= 0) {
      ERR_print_errors_fp(stderr);
      return -1;
    }
  }
  return 0;
}

int v_read(struct epoll_conn * conn, void * data, size_t len) {
  if (conn->ssl) {
    return SSL_read(conn->ssl, data, len);
  } else {
    return read(conn->epoll.socket, data, len);
  }
}

int v_write(struct epoll_conn * conn, const void * data, size_t len) {
  if (conn->ssl) {
    return SSL_write(conn->ssl, data, len);
  } else {
    return write(conn->epoll.socket, data, len);
  }
}

int v_close(struct epoll_conn * conn) {
  if (conn->ssl) {
    SSL_shutdown(conn->ssl); // Failure here is OK - likely due to server disconnect
    SSL_free(conn->ssl);
    conn->ssl = NULL;
  }

  if (close(conn->epoll.socket) < 0) {
    perror("Socket close failed");
    return -1;
  }
  conn->epoll.socket = 0;

  return 0;
}

void v_epoll_mod_enable(int epoll, struct epoll_entry * entry, int epoll_flags) {
  if (entry->events & epoll_flags) return;
  entry->events |= epoll_flags;

  struct epoll_event ev = {
    .data.ptr = entry,
    .events   = entry->events,
  };

  epoll_ctl(epoll, EPOLL_CTL_MOD, entry->socket, &ev);
}

void v_epoll_mod_disable(int epoll, struct epoll_entry * entry, int epoll_flags) {
  if (~entry->events & epoll_flags) return;
  entry->events &= ~epoll_flags;

  struct epoll_event ev = {
    .data.ptr = entry,
    .events   = entry->events,
  };

  epoll_ctl(epoll, EPOLL_CTL_MOD, entry->socket, &ev);
}

void v_epoll_add(int epoll, struct epoll_entry * entry, int epoll_flags) {
  entry->events         = epoll_flags;
  struct epoll_event ev = {
    .data.ptr = entry,
    .events   = entry->events,
  };

  epoll_ctl(epoll, EPOLL_CTL_ADD, entry->socket, &ev);
}

void v_epoll_del(int epoll, struct epoll_entry * entry) {
  epoll_ctl(epoll, EPOLL_CTL_DEL, entry->socket, NULL);
}

int v_epoll_run_loop(int epoll) {
  struct epoll_event events[EPOLL_LOOP_BATCH_SIZE];

  while (1) {
    int num_events = epoll_wait(epoll, events, EPOLL_LOOP_BATCH_SIZE, -1);
    if (num_events < 0) {
      perror("Epoll wait failed");
      return -1;
    }

    if (num_events == 0) {
      fprintf(stderr, "Epoll returned no fds\n");
      return -1;
    }

    for (int i = 0; i < num_events; i++) {
      struct epoll_entry * entry = (struct epoll_entry *) events[i].data.ptr;
      if (events[i].events & (EPOLLRDHUP)) {
        printf("RDHUP!\n");
        exit(1);
      }
      
      if (events[i].events & EPOLLHUP) {
        entry->hup(epoll, events[i].data.ptr);
      } else if (events[i].events & EPOLLIN && entry->in) {
        entry->in(epoll, events[i].data.ptr);
      } else if (events[i].events & EPOLLOUT && entry->out) {
        entry->out(epoll, events[i].data.ptr);
      }
    }
  }
}