#include "v_sock.h"

char read_buffer[MAX_SSL_RECORD];

void echo_in(int epoll, struct epoll_conn * conn) {
  int len = v_read(conn, read_buffer, MAX_SSL_RECORD);
  printf("Read: %d\n", len);
  if (len == 0) {
    v_epoll_del(epoll, &conn->epoll);
    v_close(conn);
    free(conn);
    printf("Closed\n");
    return;
  }
  if (len != v_write(conn, read_buffer, len)) {
    printf("Buffering occurred\n");
  }
}
void echo_out() {}

void echo_hup(int epoll, struct epoll_conn * conn) {
  v_epoll_del(epoll, &conn->epoll);
  v_close(conn);
  free(conn);
  printf("HUP Closed\n");
  return;
}

void echo_accept(int epoll, struct epoll_server * server) {
  struct epoll_conn * conn = malloc(sizeof(struct epoll_conn));
  if (v_accept(server, conn) < 0) {
    free(conn);
    return;
  }

  conn->epoll.in = (epoll_callback) echo_in;
  conn->epoll.out = (epoll_callback) echo_out;
  conn->epoll.hup = (epoll_callback) echo_hup;

  v_epoll_add(epoll, &conn->epoll, EPOLLIN);
  return;
}

int main(const int argc, const char* argv[]) {
  if (argc < 2) {
    fprintf(stderr, "Usage: echo_server <port> [ssl]\n");
    return -1;
  }

  int port = atoi(argv[1]);
  printf("Listening on port %d\n", port);

  bool ssl = false;
  if (argc >= 3 && strcmp("ssl", argv[2]) == 0) {
    ssl = true;
    printf("SSL Enabled\n");
  }

  struct epoll_server server;

  if (v_listen(&server, ssl ? "cert.crt" : NULL, "cert.key", port) < 0) return -1;

  int epoll;
  if ((epoll = epoll_create(1)) < 0) {
    perror("Epoll create failed");
    return -1;
  }

  server.epoll.in = (epoll_callback) echo_accept;
  v_epoll_add(epoll, &server.epoll, EPOLLIN | EPOLLHUP | EPOLLRDHUP);
  v_epoll_run_loop(epoll);

  return 0;
}
