// Reference https://github.com/openssl/openssl/blob/master/demos/sslecho/main.c

#include "v_sock.h"
#include <errno.h>
#include <sys/epoll.h>
#include <stdbool.h>


#define MAX_SSL_RECORD 16384
char read_buffer[MAX_SSL_RECORD];

struct app_server {
  struct epoll_server epoll_server;
  struct app_socket * app_socket;
  char * write_in;
  char * write_out;
  char write_buffer[MAX_SSL_RECORD];
};

struct app_client {
  struct epoll_client epoll_client;
  struct app_socket * app_socket;
  char * read_in;
  char * read_out;
  char * write_in;
  char * write_out;
  char read_buffer[MAX_SSL_RECORD];
  char write_buffer[MAX_SSL_RECORD];
};

#define REDUNDANCY 3
struct app_socket {
  struct app_server server;
  struct app_client clients[REDUNDANCY];
};

/**
 * Return the index of the first buffered client, or -1 if no client is currently buffered for writing.
 **/
int get_buffered_client_write(struct app_socket * app_socket) {
  for (int i = 0; i < REDUNDANCY; i++) {
    if (app_socket->clients[i].write_in != app_socket->clients[i].write_buffer) {
      return i;
    }
  }

  return -1;
}

void app_shutdown(int epoll, struct app_socket * app_socket) {
  v_epoll_socket_del(epoll, &app_socket->server);
  for (int i = 0; i < REDUNDANCY; i++) {
    v_epoll_client_del(epoll, &app_socket->clients[i]);
  }
}

void app_server_in(int epoll, struct app_server * app_server) {
  struct app_socket * app_socket = app_server->app_socket;

  int buffered_client = get_buffered_client_write(app_socket);
  if (buffered_client == -1) {
    // OK, normal operation. No client is buffered.
    // Read from the server socket one SSL record
    int rlen = v_read(&app_server->epoll_server.server.socket, app_socket->clients[0].write_buffer, MAX_SSL_RECORD);
    if (rlen == 0) {
      // Connection closed, close everything down.
      // v_epoll_socket_del(epoll, socket);
      // v_accept_close(&socket->socket);
      // free(socket);
      // printf("Closed\n");
      return;
    }

    // Attempt to write the SSL record to each client, and if there are any leftovers, then
    // put them in the write buffer.
    for (int i = 0; i < REDUNDANCY; i++) {
      int wlen = v_write(&app_socket->clients[i].epoll_client.client.socket, app_socket->clients[0].write_buffer, rlen);
      if (wlen != rlen) {
        printf("wlen (%d) != rlen (%d)\n");
        printf("Buffering!\n");
        if (i != 0) {
          memcpy(app_socket->clients[i].write_buffer, app_socket->clients[0].write_buffer, rlen);
        }
        app_socket->clients[i].write_in = app_socket->clients[i].write_buffer + rlen;
        app_socket->clients[i].write_out = app_socket->clients[i].write_buffer + wlen;
      }
    }
  }
}
  


void test_client_out() {}

void test_server_in(int epoll, struct epoll_socket * socket) {
  int len = v_read(&socket->socket, read_buffer, MAX_SSL_RECORD);
  printf("Read: %d\n", len);
  if (len == 0) {
    v_epoll_socket_del(epoll, socket);
    v_accept_close(&socket->socket);
    free(socket);
    printf("Closed\n");
    return;
  }
  v_write(&socket->socket, read_buffer, len);
}
void test_server_out() {}

void app_accept(int epoll, struct epoll_server * server) {
  struct app_socket * socket = malloc(sizeof(struct app_socket));
  if (v_accept(&server->server, &socket->client.socket) < 0) return;

  socket->client.epoll_in = app_client_in;
  socket->client.epoll_out = app_client_out;
  v_epoll_socket_add(epoll, &socket->client, 1, 0);

  for (int i = 0; i < REDUNDANCY; i++) {
    socket->servers[i].epoll_in = test_socket_in;
    socket->servers[i].epoll_out = test_socket_out;
    v_epoll_client_add(epoll, &socket->servers[i], 1, 0);
  }

  return;
}


int main(const int argc, const char* argv[]) {
  if (argc != 2) {
    fprintf(stderr, "Usage: echo_server <i>");
    return -1;
  }

  int port = 3000 + atoi(argv[1]);
  printf("Listening on port %d\n", port);

  struct epoll_server server;
  char data[256];

  if (v_listen(&server.server, NULL, "cert.key", port) < 0) return -1;

  int epoll;
  if ((epoll = epoll_create(1)) < 0) {
    perror("Epoll create failed");
    return -1;
  }

  server.epoll_in = test_accept;
  v_epoll_server_add(epoll, &server);
  v_epoll_loop(epoll);

  return 0;
}








