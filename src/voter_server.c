#include "v_sock.h"

#define NUM_CLIENTS 3

struct voter_client_config {
  const char * host;
  int port;
  SSL_CTX * ssl_ctx;
};

struct voter_config {
  SSL_CTX * client_ssl_ctx;
  struct voter_client_config client[NUM_CLIENTS];
} config;

char read_buffer[MAX_SSL_RECORD];

struct voter_instance;

struct voter_conn {
  struct epoll_conn conn;
  struct voter_instance * voter;
  char read_buffer[MAX_SSL_RECORD];
  char * read_ptr;
  uint32_t read_len;
  uint32_t backlog_len;
  bool is_open; // Both ends are fully open, good connection
  bool is_closed; // Both ends fully closed, do not write.
};

struct voter_instance {
  struct voter_conn server;
  struct voter_conn client[NUM_CLIENTS];
};

bool voter_close_conn(int epoll, struct voter_conn * voter_conn) {
  // Silence the input callback
  if (voter_conn->is_open) {
    v_epoll_mod_disable(epoll, &voter_conn->conn.epoll, EPOLLIN);
    voter_conn->is_open = false;
  }

  // Check if the entire instance is closed
  bool clients_open = 0;
  for (int i = 0; i < NUM_CLIENTS; i++) {
    clients_open += voter_conn->voter->client[i].is_open;
  }

  // If the server is EOF and more than half of the clients are EOF, close the connection completely.
  if (!voter_conn->voter->server.is_open && (clients_open < (NUM_CLIENTS + 1) / 2)) {
    v_epoll_del(epoll, &voter_conn->voter->server.conn.epoll);
    v_close(&voter_conn->voter->server.conn);
    for (int i = 0; i < NUM_CLIENTS; i++) {
      if (voter_conn->voter->client[i].is_open) v_epoll_del(epoll, &voter_conn->voter->client[i].conn.epoll);
      v_close(&voter_conn->voter->client[i].conn);
    }
    free(voter_conn->voter);
    printf("Closed\n");
    return true;
  }
  return false;
}

void voter_server_in(int epoll, struct voter_conn * server_conn) {
  int len = v_read(&server_conn->conn, read_buffer, MAX_SSL_RECORD);
  printf("Read: %d\n", len);
  if (len == 0) {
    printf("Server closed\n");
    voter_close_conn(epoll, server_conn);
    return;
  }

  for (int i = 0; i < NUM_CLIENTS; i++) {
    if (server_conn->voter->client[i].is_closed) continue;
    if (len != v_write(&server_conn->voter->client[i].conn, read_buffer, len)) {
      printf("Buffering occurred\n");
      if (voter_close_conn(epoll, &server_conn->voter->client[i])) return;
    }
  }
}

void voter_client_in(int epoll, struct voter_conn * client_conn) {
  client_conn->read_len = v_read(&client_conn->conn, client_conn->read_buffer, MAX_SSL_RECORD);
  printf("Client Read: %d\n", client_conn->read_len);
  if (client_conn->read_len == 0) {
    printf("Client closed\n");
    voter_close_conn(epoll, client_conn);
    return;
  }

  // There is a backlog, and this read was not enough
  if (client_conn->backlog_len > client_conn->read_len) {
    client_conn->backlog_len -= client_conn->read_len;
    client_conn->read_len = 0;
    return;
  }

  // There is a backlog, but we read more than the backlog amount
  if (client_conn->backlog_len > 0) {
    client_conn->read_ptr += client_conn->backlog_len;
    client_conn->read_len -= client_conn->backlog_len;
    client_conn->backlog_len = 0;
  }

  // VOTE!
#if NUM_CLIENTS != 3
#  error "Voter only supports 3 clients currently"
#endif

  struct voter_instance * voter = client_conn->voter;

  // First, sort the clients by their read_len, largest to smallest
  struct voter_conn * sorted_clients[NUM_CLIENTS];
  for (int i = 0; i < NUM_CLIENTS; i++) {
    int j = i;
    for (; j > 0; j--) {
      if (sorted_clients[j-1]->read_len < voter->client[i].read_len)
        sorted_clients[j] = sorted_clients[j-1];
      else
        break;
    }
    sorted_clients[j] = &voter->client[i];
  }

  int three_len = sorted_clients[2]->read_len;
  int two_len = sorted_clients[1]->read_len;

  int i = 0;

  for (; i < three_len; i++) {
    // There are three responses, do any two agree?
    if (voter->client[1].read_ptr[i] == voter->client[2].read_ptr[i]) {
      sorted_clients[0]->read_ptr[i] = voter->client[2].read_ptr[i];
    } else if (voter->client[0].read_ptr[i] == voter->client[2].read_ptr[i]) {
      sorted_clients[0]->read_ptr[i] = voter->client[2].read_ptr[i];
    } else { // Default to 0 on complete disagreement
      sorted_clients[0]->read_ptr[i] = voter->client[0].read_ptr[i];
    }
  }

  // If one of the clients is closed and the other two disagree, default to the one with the most data now being right.
  // read_len = 0 for closed clients, so it will be in [2].
  if (!sorted_clients[2]->is_open) {
    i = two_len + three_len;
  } else {
    for (; i < two_len + three_len; i++) {
      // If these don't agree, stop and wait for the third.
      if (sorted_clients[0]->read_ptr[i] != sorted_clients[1]->read_ptr[i]) break;
    }
  }

  // There is data to send
  if (i > 0) {
    if (i != v_write(&voter->server.conn, sorted_clients[0]->read_ptr, i)) {
      printf("Buffering occurred or write failure\n");
      if (voter_close_conn(epoll, &voter->server)) return;
    }
  }

  // Update all pointers and lengths
  for (int j = 0; j < NUM_CLIENTS; j++) {
    if (!voter->client[j].is_open) continue;
    if (voter->client[j].read_len > i) {
      voter->client[j].read_len -= i;
      voter->client[j].read_ptr += i;
      v_epoll_mod_disable(epoll, &voter->client[j].conn.epoll, EPOLLIN);
    } else {
      voter->client[j].backlog_len += (i - voter->client[j].read_len);
      voter->client[j].read_len = 0;
      voter->client[j].read_ptr = voter->client[j].read_buffer;
      v_epoll_mod_enable(epoll, &voter->client[j].conn.epoll, EPOLLIN);
    }
  }  
}

void voter_out(int epoll, struct voter_conn * voter_conn) {}

void voter_client_hup(int epoll, struct voter_conn * voter_conn) {
  voter_conn->read_len = 0;
  v_epoll_del(epoll, &voter_conn->conn.epoll);
  v_close(&voter_conn->conn);
  voter_conn->is_closed = true;
  voter_close_conn(epoll, voter_conn);
  printf("HUP Closed\n");
}

void voter_server_hup(int epoll, struct voter_conn * voter_conn) {
  v_epoll_del(epoll, &voter_conn->voter->server.conn.epoll);
  v_close(&voter_conn->voter->server.conn);
  for (int i = 0; i < NUM_CLIENTS; i++) {
    v_epoll_del(epoll, &voter_conn->voter->client[i].conn.epoll);
    v_close(&voter_conn->voter->client[i].conn);
  }
  free(voter_conn->voter);
  printf("HUP Closed\n");
}

void voter_accept(int epoll, struct epoll_server * server) {
  struct voter_instance * voter = malloc(sizeof(struct voter_instance));
  if (v_accept(server, &voter->server.conn) < 0) {
    free(voter);
    return;
  }

  // Attempt to connect to voter destination
  int failures = 0;
  for (int i = 0; i < NUM_CLIENTS; i++) {
    if (v_connect(&voter->client[i].conn, config.client[i].ssl_ctx, config.client[i].host, config.client[i].port) < 0) {
      failures++;
      voter->client[i].is_closed = true;
      voter->client[i].is_open = false;
      if (failures > NUM_CLIENTS/2) {
        printf("Could not open enough endpoints\n");
        // Can't open enough clients. Close connection completely
        v_close(&voter->server.conn);
        for (int j = 0; j < i; j++) {
          if (!voter->client[j].is_closed) v_close(&voter->client[j].conn);
        }
        return;
      }
    } else {
      voter->client[i].is_open = true;
      voter->client[i].is_closed = false;
    }
  }

  voter->server.voter          = voter;
  voter->server.is_open        = true;
  voter->server.conn.epoll.in  = (epoll_callback) voter_server_in;
  voter->server.conn.epoll.out = (epoll_callback) voter_out;
  voter->server.conn.epoll.hup = (epoll_callback) voter_server_hup;
  v_epoll_add(epoll, &voter->server.conn.epoll, EPOLLIN);

  for (int i = 0; i < NUM_CLIENTS; i++) {
    voter->client[i].voter          = voter;
    voter->client[i].read_ptr       = voter->client[i].read_buffer;
    voter->client[i].read_len       = 0;
    voter->client[i].backlog_len    = 0;
    voter->client[i].conn.epoll.in  = (epoll_callback) voter_client_in;
    voter->client[i].conn.epoll.out = (epoll_callback) voter_out;
    voter->client[i].conn.epoll.hup = (epoll_callback) voter_client_hup;
    if (voter->client[i].is_open) v_epoll_add(epoll, &voter->client[i].conn.epoll, EPOLLIN);
  }
  return;
}

int help() {
  fprintf(stderr, "Usage: voter_server <listen_port> [ssl] <connect_host> <connect_port> [ssl] ...\n");
  return 1;
}

int main(int argc, const char * argv[]) {
  argv++;
  if (*argv == NULL) return help();
  int port = atoi(*argv++);

  if (*argv == NULL) return help();
  bool ssl = false;
  if (strcmp("ssl", *argv) == 0) {
    argv++;
    ssl = true;
    printf("Listen SSL Enabled\n");
  }

  for (int i = 0; i < NUM_CLIENTS; i++) {
    if (*argv == NULL) return help();
    config.client[i].host = *argv++;

    if (*argv == NULL) return help();
    config.client[i].port = atoi(*argv++);

    config.client[i].ssl_ctx = NULL;
    if (*argv != NULL && strcmp("ssl", *argv) == 0) {
      argv++;
      if (!config.client_ssl_ctx) {
        config.client_ssl_ctx = client_ssl_ctx_create();
      }
      config.client[i].ssl_ctx = config.client_ssl_ctx;
      printf("Connect %d SSL Enabled\n", i);
    }
  }

  struct epoll_server server;

  if (v_listen(&server, ssl ? "cert.crt" : NULL, "cert.key", port) < 0) return -1;

  printf("Listening on port %d\n", port);

  int epoll;
  if ((epoll = epoll_create(1)) < 0) {
    perror("Epoll create failed");
    return -1;
  }

  server.epoll.in = (epoll_callback) voter_accept;
  v_epoll_add(epoll, &server.epoll, EPOLLIN | EPOLLHUP | EPOLLRDHUP);
  v_epoll_run_loop(epoll);

  return 0;
}