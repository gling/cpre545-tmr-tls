#include "v_sock.h"

struct forward_config {
  SSL_CTX * client_ssl_ctx;
  const char * client_host;
  int client_port;
} config;

char read_buffer[MAX_SSL_RECORD];


struct forward_instance;

struct forward_conn {
  struct epoll_conn conn;
  struct forward_instance * fwd;
  bool is_open;
};

struct forward_instance {
  struct forward_conn server;
  struct forward_conn client;
};

bool forward_conn_is_server(struct forward_conn * fwd_conn) {
  return &fwd_conn->fwd->server == fwd_conn;
}

struct forward_conn * forward_conn_get_pair(struct forward_conn * fwd_conn) {
  return forward_conn_is_server(fwd_conn) ? &fwd_conn->fwd->client : &fwd_conn->fwd->server;
}

void forward_in(int epoll, struct forward_conn * fwd_conn) {
  printf("FWD: %p Client: %p Server: %p\n", fwd_conn->fwd, &fwd_conn->fwd->client, &fwd_conn->fwd->server);

  struct forward_conn * pair = forward_conn_get_pair(fwd_conn);

  int len = v_read(&fwd_conn->conn, read_buffer, MAX_SSL_RECORD);
  printf("Read: %d\n", len);
  if (len == 0) {
    if (forward_conn_is_server(fwd_conn)) {
      printf("Server is closed\n");
    } else {
      printf("Client is closed\n");
    }

    
    v_epoll_mod_disable(epoll, &fwd_conn->conn.epoll, EPOLLIN);
    
    fwd_conn->is_open = false;

    // Once both are closed, shut down the connection.
    if (!pair->is_open) {
      v_epoll_del(epoll, &fwd_conn->conn.epoll);
      v_epoll_del(epoll, &pair->conn.epoll);
      v_close(&fwd_conn->conn);
      v_close(&pair->conn);
      free(fwd_conn->fwd);
      printf("Closed\n");
    }
    return;
  }
  if (len != v_write(&pair->conn, read_buffer, len)) {
    printf("Buffering occurred\n");
  }
}

void forward_out(int epoll, struct forward_conn * fwd_conn) {}

void forward_hup(int epoll, struct forward_conn * fwd_conn) {
  // Close everything down
  struct forward_instance * fwd = fwd_conn->fwd;

  v_epoll_del(epoll, &fwd->server.conn.epoll);
  v_epoll_del(epoll, &fwd->client.conn.epoll);
  v_close(&fwd->server.conn);
  v_close(&fwd->client.conn);
  free(fwd_conn->fwd);
  printf("HUP Closed\n");
}

void forward_accept(int epoll, struct epoll_server * server) {
  struct forward_instance * fwd = malloc(sizeof(struct forward_instance));
  if (v_accept(server, &fwd->server.conn) < 0) {
    free(fwd);
    return;
  }

  // Attempt to connect to forward destination
  if (v_connect(&fwd->client.conn, config.client_ssl_ctx, config.client_host, config.client_port) < 0) {
    v_close(&fwd->server.conn);
    free(fwd);
    return;
  }

  printf("FWD: %p Client: %p Server: %p\n", fwd, &fwd->client, &fwd->server);

  fwd->server.fwd = fwd;
  fwd->server.is_open = true;
  fwd->server.conn.epoll.in = (epoll_callback) forward_in;
  fwd->server.conn.epoll.out = (epoll_callback) forward_out;
  fwd->server.conn.epoll.hup = (epoll_callback) forward_hup;
  v_epoll_add(epoll, &fwd->server.conn.epoll, EPOLLIN);

  fwd->client.fwd = fwd;
  fwd->client.is_open = true;
  fwd->client.conn.epoll.in = (epoll_callback) forward_in;
  fwd->client.conn.epoll.out = (epoll_callback) forward_out;
  fwd->client.conn.epoll.hup = (epoll_callback) forward_hup;
  v_epoll_add(epoll, &fwd->client.conn.epoll, EPOLLIN);

  return;
}

int help() {
  fprintf(stderr, "Usage: forward_server <listen_port> [ssl] <connect_host> <connect_port> [ssl]\n");
  return 1;
}

int main(int argc, const char* argv[]) {
  argv++;
  if (*argv == NULL) return help();
  int port = atoi(*argv++);
  printf("Listening on port %d\n", port);

  if (*argv == NULL) return help();
  bool ssl = false;
  if (strcmp("ssl", *argv) == 0) {
    argv++;
    ssl = true;
    printf("Listen SSL Enabled\n");
  }

  if (*argv == NULL) return help();
  config.client_host = *argv++;

  if (*argv == NULL) return help();
  config.client_port = atoi(*argv++);

  config.client_ssl_ctx = NULL;
  if (*argv != NULL && strcmp("ssl", *argv) == 0) {
    argv++;
    config.client_ssl_ctx = client_ssl_ctx_create();
    printf("Connect SSL Enabled\n");
  }

  struct epoll_server server;

  if (v_listen(&server, ssl ? "cert.crt" : NULL, "cert.key", port) < 0) return -1;

  int epoll;
  if ((epoll = epoll_create(1)) < 0) {
    perror("Epoll create failed");
    return -1;
  }

  server.epoll.in = (epoll_callback) forward_accept;
  v_epoll_add(epoll, &server.epoll, EPOLLIN | EPOLLHUP | EPOLLRDHUP);
  v_epoll_run_loop(epoll);

  return 0;
}