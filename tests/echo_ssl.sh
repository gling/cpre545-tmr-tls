#!/bin/sh

# A simple script to test the echo server functionality
cd build

make -C .. build/echo_server cert.key

# First, create a random file
dd if=/dev/random of=echo_input bs=65536 count=1 2>/dev/null

# Start the echo server (SSL enabled)
./echo_server 5000 ssl &
ECHO_PID=$!

# Wait for server to come online
sleep 0.5

# Connect to the echo server and send the random file
ncat --ssl localhost 5000 < echo_input > echo_output

# Again
ncat --ssl localhost 5000 < echo_input > echo_output2

# Kill the echo server
kill $ECHO_PID

# And check that all three match
diff -q echo_input echo_output
diff -q echo_input echo_output2
