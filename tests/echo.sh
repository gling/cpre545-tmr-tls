#!/bin/sh

# A simple script to test the echo server functionality
cd build

make -C .. build/echo_server

# First, create a random file
dd if=/dev/random of=echo_input bs=65536 count=1 2>/dev/null

# Start the echo server (SSL disabled)
./echo_server 5000 &
ECHO_PID=$!

# Wait for server to come online
sleep 0.5

# Connect to the echo server and send the random file
nc localhost 5000 < echo_input > echo_output

# Again
nc localhost 5000 < echo_input > echo_output2

# Kill the echo server
kill $ECHO_PID

# And check that all three match
diff -q echo_input echo_output
diff -q echo_input echo_output2
