#!/bin/sh

make build/voter_server

# socat tcp-listen:4001 system:"echo 1234" &
# SOCAT1=$!

# socat tcp-listen:4002 system:"echo 124" &
# SOCAT2=$!

# socat tcp-listen:4003 system:"echo 1234" &
# SOCAT3=$!

dd if=/dev/random of=large_file bs=1048576 count=1024 2>/dev/null

python3 -m http.server 3000 &
ECHO=$!

# ./build/voter_server 4000 93.184.215.14 80 93.184.215.14 80 93.184.215.14 80 &
./build/voter_server 4000 127.0.0.1 3000 127.0.0.1 3000 127.0.0.1 3000 >/dev/null &
VOTER=$!

sleep 0.5
curl -k -H "Connection: close" http://localhost:4000/large_file > test

curl -k -H "Connection: close" http://localhost:3000/large_file > test2
# for i in $(seq 1 5); do
#   curl -k -H "Connection: close" http://localhost:4000/large_file > test$i
#   diff test test$i
# done


# socat tcp-connect:127.0.0.1:4000 stdout

# kill $SOCAT1
# kill $SOCAT2
# kill $SOCAT3
kill $ECHO
kill $VOTER


# curl -k -H "Host: example.com" -H "Connection: close" https://localhost:3000 -vvv
# ./build/forward_server 3000 ssl 93.184.215.14 80
# ./build/forward_server 3000 ssl 93.184.215.14 443 ssl